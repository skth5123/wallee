from django import forms
from django.contrib.auth.forms import *
from django.contrib.auth.models import User
from website.models import *
from django.forms import ModelForm


class SignUpForm(UserCreationForm):

    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', )

class DashboardForm(ModelForm):

    name = forms.TextInput()
    address = forms.Textarea()
    dob = forms.DateField()
    phone = forms.CharField()
    country = forms.TextInput()
    class Meta:
        model = profile
        fields = ('name', 'address', 'dob', 'phone','country', )