from django.contrib.auth import authenticate, login,logout
from django.contrib.auth import models
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from website.forms import *
def home(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return render(request, 'website/home.html',{user : username})
        # Redirect to a success page.
        else:
            return redirect('/login/')


    else:
        if not request.user.is_authenticated:
            return redirect('/login/')
        else:
            return render(request, 'website/home.html')



def logout1(request):
    logout(request)
    return render(request, 'website/logout.html')


def signup1(request):
    if request.method == 'POST':
        data = {'username': request.POST['username'],'email': request.POST['email'], 'password1': request.POST['password1'],
                'password2': request.POST['password2']}
        print(data)
        form = SignUpForm(data)


        if form.is_valid():
            form.save()
            return redirect('/login/')
        else:

            return render(request, 'website/signup.html',{ 'message': 'password should satisfy the given constraints'})
    else:

        return render(request, 'website/signup.html')


def dashboard(request):
    if request.method == 'POST':
        data = {'name': request.POST['name'],'address': request.POST['address'], 'dob': request.POST['dob'],
                'phone': request.POST['phone'],'country': request.POST['country']}
        print(data)
        form = DashboardForm(data)


        if form.is_valid():
            form.save()
            return redirect('/home/')
        else:

            return render(request, 'website/dash.html',{ 'message': 'wrong'})
    else:

        return render(request, 'website/dash.html')